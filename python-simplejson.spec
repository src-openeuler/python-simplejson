%global _empty_manifest_terminate_build 0
Name:           python-simplejson
Version:        3.19.3
Release:        2
Summary:        Simple, fast, extensible JSON encoder/decoder for Python
License:        MIT
URL:            https://github.com/simplejson/simplejson
Source0:        https://files.pythonhosted.org/packages/3d/29/085111f19717f865eceaf0d4397bf3e76b08d60428b076b64e2a1903706d/simplejson-3.19.3.tar.gz

%description
Simple, fast, extensible JSON encoder/decoder for Python

%package -n python3-simplejson
Summary:        Simple, fast, extensible JSON encoder/decoder for Python
Provides:       python-simplejson
Obsoletes:      python-simplejson-help <= %{version}-%{release}
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-cffi
BuildRequires:  gcc

%description -n python3-simplejson
Simple, fast, extensible JSON encoder/decoder for Python

%prep
%autosetup -n simplejson-%{version} -p1

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

%files -n python3-simplejson
%{python3_sitearch}/*

%changelog
* Wed Mar 12 2025 wangkai <13474090681@163.com> - 3.19.3-2
- Fixed the packaging process does not include __pycache__ tests
- Obsoletes empty package python-simplejson-help

* Tue Aug 27 2024 dongqi <dongqi1@kylinos.com> - 3.19.3-1
- Update package to version 3.19.3
- Updated test & build matrix to include Python 3.13
- Dropped wheel support for Python 2.7 on macOS

* Wed Jan 10 2024 zhangxianting <zhangxianting@uniontech.com> - 3.19.2-1
- Update to 3.19.2:
- Updated test & build matrix to include Python 3.12 and use GitHub Actions as a Trusted Publisher (OIDC) https://github.com/simplejson/simplejson/pull/317

* Thu May 11 2023 yaoxin <yao_xin001@hoperun.com> - 3.19.1-1
- Update to 3.19.1

* Thu Feb 9 2023 wubijie <wubijie@kylinos.cn> - 3.18.3-1
- Update package to version 3.18.3

* Fri Dec 16 2022 chendexi <chendexi@kylinos.cn> - 3.18.0-1
- Upgrade package to version 3.18.0

* Wed May 25 2022 OpenStack_SIG <openstack@openeuler.org> - 3.17.6-1
- Upgrade the version to 3.17.6

* Fri Aug 06 2021 OpenStack_SIG <openstack@openeuler.org> - 3.17.2-1
- Upgrade version to 3.17.2

* Wed Aug 04 2021 chenyanpanHW <chenyanpan@huawei.com> - 3.17.0-2
- DESC: delete BuildRequires gdb

* Fri Jun 19 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
